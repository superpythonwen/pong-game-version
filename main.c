#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>

//注意：height和x对应, width和y对应

const int height = 20;
const int width = 30;
const int y_1 = 15;
const int y_2 = 5;

int left_1, right_1;
int ball_x, ball_y;
int left_2, right_2;
int ball_vx, ball_vy;
int loser;

void gotoxy(int x, int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}

void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor = { 1, 0 };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor);
}

void startup()
{
	left_1 = 1;
	right_1 = 6;
	ball_x = height / 2 + rand() % 5 - 2;
	ball_y = width / 2 + rand() % 5 - 2;
	left_2 = width - 5;
	right_2 = width;
	ball_vx = 1;
	ball_vy = 1;
	loser = -1;

	system("title 少年飞工作室 Pong V1.01");

	printf("Pong游戏 V1.01版本\n");
	printf("版权所有：少年飞工作室\n");
	printf("游戏规则：上面的球拍(Play1)由左右键控制，下面的球拍(Play2)由ad键控制。\n");
	printf("最先没有接住球的人失败！\n");
	printf("PS：如果你的控制台在中文状态，请按一下Shift键\n");
	system("pause");

	gotoxy(0, 0);

	for (int i = 1; i <= 80; i++)
	{
		for (int j = 1; j <= 80; j++)
		{
			printf(" ");
		}
	}

	return;
}

void show()
{
	gotoxy(0, 0);
	printf(" ");
	for (int i = 1; i <= width - 1; i++)
	{
		printf("-");
	}
	printf("\n");

	for (int i = 1; i <= height; i++)
	{
		printf("|");
		for (int j = 1; j <= width; j++)
		{
			if (i == ball_x && j == ball_y)
			{
				printf("*");
			}
			else if (i == y_1 && j >= left_1 && j <= right_1)
			{
				printf("#");
			}
			else if (i == y_2 && j >= left_2 && j <= right_2)
			{
				printf("@");
			}
			else
			{
				printf(" ");
			}
		}

		printf("|\n");
	}

	printf(" ");
	for (int i = 1; i <= width - 1; i++)
	{
		printf("-");
	}
}

void updateNoInput()
{
	static int speed = 0;
	speed++;

	if (speed == 5)
	{
		ball_x += ball_vx;
		ball_y += ball_vy;

		if (ball_y == 1 || ball_y == width)
		{
			ball_vy *= -1;
		}
		if (ball_x == y_1 && ball_y >= left_1 && ball_y <= right_1)
		{
			ball_vx *= -1;
			ball_x += ball_vx;
			ball_y += ball_vy;
		}
		if (ball_x == y_2 && ball_y >= left_2 && ball_y <= right_2)
		{
			ball_vx *= -1;
			ball_x += ball_vx;
			ball_y += ball_vy;
		}

		if (ball_x > height)
		{
			loser = 1;
		}

		if (ball_x < 1)
		{
			loser = 2;
		}

		speed = 0;
	}
}

void updateInput()
{
	if ((GetAsyncKeyState(0x41) & 0x8000) && left_1 > 1)
	{
		left_1--;
		right_1--;
	}
	if ((GetAsyncKeyState(0x44) & 0x8000) && right_1 < width)
	{
		left_1++;
		right_1++;
	}
	if ((GetAsyncKeyState(VK_LEFT) & 0x8000) && left_2 > 1)
	{
		left_2--;
		right_2--;
	}
	if ((GetAsyncKeyState(VK_RIGHT) & 0x8000) && right_2 < width)
	{
		left_2++;
		right_2++;
	}
}

int main()
{
	srand(time(0));
	HideCursor();
	startup();
	while (1)
	{
		show();
		updateNoInput();
		updateInput();
		Sleep(10);
		if (loser == 1)
		{
			printf("\nPlay2 Lost!\n");
			printf("Game Over!\n");
			break;
		}
		else if (loser == 2)
		{
			printf("\nPlay1 Lost!\n");
			printf("Game Over!\n");
			break;
		}
	}

	while (1)
	{
		;
	}

	return 0;
}